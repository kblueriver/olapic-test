var mediaEndpointUrl = 'https://api.olapic.com/media/2899395330?auth_token=0a40a13fd9d531110b4d6515ef0d6c529acdb59e81194132356a1b8903790c18&version=v2.2'

httpGetAsync(mediaEndpointUrl, replaceWidget)

/**
 * This function sends an asynchronous GET request to the specified URL and executes the callback with the HTTP response as a parameter
 * @param {string} url - the URL to which the request will be sent
 * @param {function} callback - the callback function that will be invoked when there is a response to the GET request
 */
function httpGetAsync(url, callback)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() { 
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            callback(xmlHttp.responseText);
    }
    xmlHttp.open("GET", url, true); // true for async
    xmlHttp.send(null);
}

/**
 * This function replaces the content of the original widget container with the result from the buildWidget function
 * @param {response} the HTTP response, which is used to pass the necessary data to the buildWidget function
 */
function replaceWidget(response) {
	var data = JSON.parse(response).data
	var widget = buildWidget(data)
	
	var originalContainer = document.getElementsByClassName("resources--featured-image")[0]
	originalContainer.innerHTML = ""
	originalContainer.appendChild(widget)
}

/**
 * This function handles building the HTML code for the widget. It mostly composes it from elements returned by other functions
 * @param {JSON} data - the data from which particular values are extracted
 */
function buildWidget(data) {
	var outerWidgetContainer = getOuterWidgetContainer()
	var innerWidgetContainer = getInnerWidgetContainer()
	outerWidgetContainer.appendChild(innerWidgetContainer)
	
	var username = data._embedded.uploader.username
	var userAvatarUrl = data._embedded.uploader.avatar_url
	var userDataContainer = getUserDataContainer(username, userAvatarUrl)
	innerWidgetContainer.appendChild(userDataContainer)
	
	var imageUrl = data.images.normal
	var imageNormalImg = getImageNormalImg(imageUrl)
	innerWidgetContainer.appendChild(imageNormalImg)
	
	var date = formatDate(data.date_published)
	var dateContainer = getDateContainer(date)
	innerWidgetContainer.appendChild(dateContainer)
	
	return outerWidgetContainer
}

/**
 * This function returns the outer container for the widget
 */
function getOuterWidgetContainer() {
	var outerWidgetContainer = document.createElement('div')
	outerWidgetContainer.style.backgroundColor = '#FAFAFA'
	outerWidgetContainer.style.paddingTop='20px'
	outerWidgetContainer.style.paddingBottom='20px'
	outerWidgetContainer.style.paddingLeft='25px'
	outerWidgetContainer.style.paddingRight='25px'
	outerWidgetContainer.style.maxHeight = '685px'
	outerWidgetContainer.style.maxWidth = '604px'
	return outerWidgetContainer
}

/**
 * This function returns the inner container for the widget
 */
function getInnerWidgetContainer() {
	var innerWidgetContainer = document.createElement('div')
	innerWidgetContainer.style.border = '3px solid #EBEBEB'
	innerWidgetContainer.style.borderRadius = '5px'
	return innerWidgetContainer
}

/**
 * This function returns an <img> element with its source set to the imageUrl parameter
 * @param {string} imageUrl - the url to use as src for the image
 */
function getImageNormalImg(imageUrl) {
	var imageNormalImg = document.createElement('img')
	imageNormalImg.src = imageUrl
	return imageNormalImg;
}

/**
 * This function returns the div that contains the user data, along with all user data elements
 * @param {string} username - the username for the user
 * @param {string} userAvatarUrl - the URL to use as src for the userAvatar <img> tag
 */
function getUserDataContainer(username, userAvatarUrl) {
	var userDataContainer = document.createElement('div')
	userDataContainer.style.backgroundColor = '#FFFFFF'
	
	var userAvatar = document.createElement('img')
	userAvatar.src = userAvatarUrl
	userAvatar.style.height = '60px'
	userAvatar.style.width = '60px'
	userAvatar.style.display = 'inline-block'
	userAvatar.style.marginTop = '15px'
	userAvatar.style.marginBottom = '15px'
	userAvatar.style.marginLeft = '15px'
	userAvatar.style.marginRight = '15px'
	
	var userNameContainer = document.createElement('div')
	userNameContainer.style.display = 'inline-block'
	userNameContainer.style.position = 'absolute'
	userNameContainer.style.marginTop = '28px'
	userNameContainer.style.fontFamily = 'arial'
	userNameContainer.style.fontWeight = 'bold'
	userNameContainer.style.fontSize = '25px'
	
	var userNameTextNode = document.createTextNode(username)
	userNameContainer.appendChild(userNameTextNode)
	
	userDataContainer.appendChild(userAvatar)
	userDataContainer.appendChild(userNameContainer)
	return userDataContainer
}

/**
 * This function returns the <div> that contains the date element of the widget
 * @param {string} date - the date to use, already formatted
 */
function getDateContainer(date) {
	var dateContainer = document.createElement('div')
	dateContainer.style.backgroundColor = '#FFFFFF'
	dateContainer.style.height = '100%'
	dateContainer.style.paddingTop = '40px'
	dateContainer.style.paddingBottom = '40px'
	dateContainer.style.paddingLeft = '20px'
	
	var dateTextContainer = document.createElement('div')
	dateTextContainer.style.color = '#AAAAAA'
	dateTextContainer.style.fontFamily = 'arial'
	dateTextContainer.style.fontWeight = 'bold'
	dateTextContainer.style.fontSize = '20px'
	
	var dateTextNode = document.createTextNode(date)
	dateTextContainer.appendChild(dateTextNode)
	
	dateContainer.appendChild(dateTextContainer)
	return dateContainer
}

/**
 * This function converts from the typical string representation of a date to a yyyy-MM-dd format
 * @param {string} dateString - string representation of the date to be formatted
 */
function formatDate(dateString) {
	var date = new Date(dateString);

	var year = date.getFullYear();
	var month = date.getMonth()+1;
	var day = date.getDate();

	if (day < 10) {
	  day = '0' + day;
	}
	if (month < 10) {
	  month = '0' + month;
	}

	return year + '-' + month + '-' + day
}


/*
let script = document.createElement('script')
script.src = 'https://s3.amazonaws.com/nullknowledgeexception/js/script.js'
document.head.appendChild(script)
*/