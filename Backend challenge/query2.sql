SELECT ROUND(SUM(amount), 2) AS 'Revenue (USD)',
	COUNT(product_id) AS 'Number of products sold'
FROM hits
WHERE event_id=1
