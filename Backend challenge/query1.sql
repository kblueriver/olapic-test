SELECT concat(date_format(ts, '%Y-%m-%d'), ' from ', date_format(ts, '%H:00'), ' to ', date_format(ts, '%H') + 1, ':00') as 'Hour',
	widget_events.name as 'Type of event',
    COUNT(*) as 'Count'
FROM hits
JOIN widget_events ON hits.event_id = widget_events.id
GROUP BY date_format(ts, '%Y-%m-%d %H'), event_id;