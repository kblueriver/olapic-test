SELECT ROUND(SUM(amount), 2) AS 'Revenue (USD)',
	COUNT(product_id) AS 'Number of products sold'
FROM hits recurrenthits
WHERE event_id = 1
AND user_id IN (
	SELECT user_id FROM hits previoushits
    WHERE previoushits.ts < recurrenthits.ts
    AND previoushits.event_id = 11)
